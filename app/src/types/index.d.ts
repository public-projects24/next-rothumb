declare global {
    type ViewType = "grid" | "list";
    type Votes = {
        positive: number;
        negative: number;
    };
    type Person = {
        name: string;
        description: string;
        category: string;
        picture: string;
        lastUpdated: string;
        votes: Votes;
    };
}

export {};
