import "@/assets/css/components/common/people-card.css";
import timeAgo from "@/services/timeAgo";
import { useState } from "react";
import { incrementVote } from "@/store/peopleSlice";
import { useAppDispatch } from "@/hooks/reduxHooks";

type propsType = {
    viewType: ViewType;
    person: Person;
};
export default function PeopleCard(props: propsType) {
    const dispatch = useAppDispatch();
    const lastUpdated = timeAgo(new Date(props.person.lastUpdated));
    const totalVotes =
        props.person.votes.positive + props.person.votes.negative;

    const calculatePercentage = (votes: number) => {
        return ((votes * 100) / totalVotes).toFixed(1);
    };
    const positivePercentage = calculatePercentage(props.person.votes.positive);
    const negativePercentage = calculatePercentage(props.person.votes.negative);

    const [isThumbPositiveActive, setIsThumbPositiveActive] = useState(false);
    const [isThumbNegativeActive, setIsThumbNegativeActive] = useState(false);
    const [isVoteNowDisabled, setIsVoteNowDisabled] = useState(true);
    const [isVotePosted, setIsVotePosted] = useState(false);

    const handleThumbButtonClick = (vote: "positive" | "negative") => {
        if (vote === "positive") {
            setIsThumbPositiveActive(true);
            setIsThumbNegativeActive(false);
        } else {
            setIsThumbPositiveActive(false);
            setIsThumbNegativeActive(true);
        }
        setIsVoteNowDisabled(false);
    };
    const handleVoteNow = () => {
        if (!isVotePosted) {
            dispatch(
                incrementVote({
                    personName: props.person.name,
                    voteType: isThumbPositiveActive ? "positive" : "negative",
                })
            );
            setIsVotePosted(true);
        } else {
            setIsThumbPositiveActive(false);
            setIsThumbNegativeActive(false);
            setIsVoteNowDisabled(true);
            setIsVotePosted(false);
        }
    };

    return (
        <li
            className={`people-card ${
                props.viewType == "list" ? "people-card--list" : ""
            }`}
        >
            <picture>
                <source
                    media="(max-width: 767px)"
                    srcSet={`/img/people/mobile/${props.person.picture}`}
                />
                <source
                    media="(min-width: 1100px)"
                    srcSet={`/img/people/desktop/${props.viewType}/${props.person.picture}`}
                />
                <img
                    src={`/img/people/tablet/${props.viewType}/${props.person.picture}`}
                    alt=""
                    className="people-card__image "
                />
            </picture>

            <div className="people-card__cover">
                <div className="people-card__content">
                    <div className="people-card__content-left">
                        <img
                            src={`/img/people/${
                                positivePercentage > negativePercentage
                                    ? "thumbs-up.svg"
                                    : "thumbs-down.svg"
                            }`}
                            alt=""
                        />
                    </div>
                    <div className="people-card__content-right">
                        <div className="people-card__info">
                            <div className="people-card__title">
                                {props.person.name}
                            </div>
                            <div className="people-card__excerpt-box">
                                <div className="people-card__excerpt-container">
                                    <div className="people-card__excerpt">
                                        {props.person.description.substring(
                                            0,
                                            126
                                        )}
                                        ...
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="people-card__actions">
                            <div className="people-card__date">
                                {isVotePosted
                                    ? "Thank you for your vote!"
                                    : `${lastUpdated} in ${props.person.category}`}
                            </div>
                            <div className="people-card__buttons">
                                <img
                                    src="/img/people/thumbs-up.svg"
                                    alt="thumbs-up"
                                    className={`people-card__thumb-button ${
                                        isThumbPositiveActive
                                            ? "people-card__thumb-button--active"
                                            : ""
                                    }`}
                                    onClick={() =>
                                        handleThumbButtonClick("positive")
                                    }
                                />
                                <img
                                    src="/img/people/thumbs-down.svg"
                                    alt="thumbs-down"
                                    className={`people-card__thumb-button ${
                                        isThumbNegativeActive
                                            ? "people-card__thumb-button--active"
                                            : ""
                                    }`}
                                    onClick={() =>
                                        handleThumbButtonClick("negative")
                                    }
                                />
                                <button
                                    className={`people-card__btn-vote-now ${
                                        !isVoteNowDisabled
                                            ? "people-card__btn-vote-now--active"
                                            : ""
                                    }`}
                                    onClick={handleVoteNow}
                                    disabled={isVoteNowDisabled}
                                >
                                    {isVotePosted ? "Vote Again" : "Vote Now"}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="people-card__progress-bar">
                    <div className="people-card__percentage-bars">
                        <div
                            className="people-card__percentage-blue-bar"
                            style={{ width: `${positivePercentage}%` }}
                        ></div>
                        <div
                            className="people-card__percentage-orange-bar"
                            style={{ width: `${negativePercentage}%` }}
                        ></div>
                    </div>
                    <div className="people-card__percentage-items">
                        <div className="people-card__percentage-item">
                            <img
                                src="/img/people/thumbs-up-individual.svg"
                                alt=""
                            />
                            <span className="people-card__percentage-label">
                                {positivePercentage}%
                            </span>
                        </div>
                        <div className="people-card__percentage-item">
                            <span className="people-card__percentage-label">
                                {negativePercentage}%
                            </span>
                            <img
                                src="/img/people/thumbs-down-individual.svg"
                                alt=""
                            />
                        </div>
                    </div>
                </div>
            </div>
        </li>
    );
}
