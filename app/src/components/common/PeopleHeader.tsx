import "@/assets/css/components/common/people-header.css";
import DropDown from "./DropDown";

export default function PeopleHeader() {
    return (
        <div className="people-header">
            <div>
                <h3 className="people-header__title">Previous Rulings</h3>
            </div>
            <div>
                <DropDown />
            </div>
        </div>
    );
}
