"use client";
import "@/assets/css/components/common/people.css";
import { useEffect, useRef } from "react";
import PeopleCard from "./PeopleCard";
import { initPersons } from "@/store/peopleSlice";
import { useAppDispatch, useAppSelector } from "@/hooks/reduxHooks";
import PeopleHeader from "./PeopleHeader";
import api from "@/services/api";

export default function People() {
    const people = useAppSelector((state) => state.people);
    const dispatch = useAppDispatch();

    const peopleListRef = useRef<HTMLUListElement>(null);
    let activeDrag = false;

    useEffect(() => {
        const getPersons = async () => {
            const response = await api.get("persons");
            dispatch(initPersons(response.data.persons));
        };
        getPersons();
    }, []);

    useEffect(() => {
        let mouseDownEvent = () => {
            activeDrag = true;
        };

        let mouseUpEvent = () => {
            activeDrag = false;
        };

        let mouseMoveEvent = (drag: any) => {
            if (!activeDrag) {
                return;
            }
            if (peopleListRef.current) {
                peopleListRef.current.scrollLeft -= drag.movementX;
            }
        };

        peopleListRef.current?.addEventListener("mousedown", mouseDownEvent);
        peopleListRef.current?.addEventListener("mouseup", mouseUpEvent);
        peopleListRef.current?.addEventListener("mousemove", mouseMoveEvent);

        return () => {
            peopleListRef.current?.removeEventListener(
                "mousedown",
                mouseDownEvent
            );
            peopleListRef.current?.removeEventListener("mouseup", mouseUpEvent);
            peopleListRef.current?.addEventListener(
                "mousemove",
                mouseMoveEvent
            );
        };
    }, []);

    return (
        <main role="main">
            <PeopleHeader />

            {people.persons && (
                <div
                    className={`people ${
                        people.viewType === "list" ? "people--list" : ""
                    }`}
                >
                    <ul className="people__list" ref={peopleListRef}>
                        {people.persons.map((person, index) => (
                            <PeopleCard
                                viewType={people.viewType}
                                person={person}
                                key={index}
                            />
                        ))}
                    </ul>
                </div>
            )}
        </main>
    );
}
