import type { Config } from "tailwindcss";

const config: Config = {
    content: [
        "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    ],
    theme: {
        extend: {
            colors: {
                primary: "#fe696a",
            },
            fontFamily: {
                body: ["Rubik"],
            },
            spacing: {
                11.5: "2.875rem",
                15: "3.75rem",
                125: "31.25rem",
            },
        },
        container: {
            padding: {
                DEFAULT: "1.25rem",
                sm: "0",
                lg: "0",
                xl: "0",
                "2xl": "0",
            },
        },
    },
    plugins: [],
};
export default config;
